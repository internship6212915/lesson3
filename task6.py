from math import sqrt


def main():
    while True:
        num = int(input("Введите число: "))
        print("Простое") if is_prime(num) else print("Составное")


def is_prime(num: int) -> bool:
    for divider in range(2, int(sqrt(num) + 1)):
        if num % divider == 0:
            return False
    return True


if __name__ == "__main__":
    main()
