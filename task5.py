products_list = ["Молоко", "Рис", "Яблоки"]


def main():
    for product in products_list:
        print(product) if len(product) % 2 == 0 else None


if __name__ == "__main__":
    main()
