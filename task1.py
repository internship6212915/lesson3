MONTHS = {
    1: "Января",
    2: "Февраля",
    3: "Марта",
    4: "Апреля",
    5: "Мая",
    6: "Июня",
    7: "Июля",
    8: "Августа",
    9: "Сентября",
    10: "Октября",
    11: "Ноября",
    12: "Декабря",
}

day = 1
month = 5

temp = -22
measure_unit_str = "градусов"

if 1 < abs(temp) % 10 < 4 and abs(temp) not in list(range(10, 21)):
    measure_unit_str = "градуса"
elif abs(temp) % 10 == 1 and abs(temp) not in list(range(10, 21)):
    measure_unit_str = "градус"


def main():
    print(
        f"Сегодня {day} {MONTHS[month]}. "
        f"На улице {temp} {measure_unit_str}"
    )
    print("Холодно, лучше остаться дома") if temp < 0 else None


if __name__ == "__main__":
    main()
