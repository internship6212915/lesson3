def main():
    while True:
        n = input("Введите номер числа в ряду числе Фибоначчи: ")
        n = int(n)
        print(fibonacci(n))


def fibonacci(num_in_row: int) -> int:
    row = [1, 1]
    row += [0] * (num_in_row - 2)
    i = 2

    while i <= (num_in_row - 1):
        row[i] = row[i - 1] + row[i - 2]
        i += 1

    return row[num_in_row - 1]


if __name__ == "__main__":
    main()
