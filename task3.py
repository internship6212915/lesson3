products_list = ["Молоко", "Хлеб", "Яблоки"]


def main():

    count = len(products_list)
    items_str = "продуктов"
    if 1 < count % 10 < 4 and count not in list(range(10, 21)):
        items_str = "продукта"
    elif count % 10 == 1 and count not in list(range(10, 21)):
        items_str = "продукт"

    print(f"У тебя {count} {items_str}")


if __name__ == "__main__":
    main()
