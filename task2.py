from math import trunc


SPEED_KMH = 1079252848.8


def main():
    speed_kms = trunc(SPEED_KMH / 3600)
    print(f"Скорость света равна {speed_kms} км/с")


if __name__ == "__main__":
    main()
